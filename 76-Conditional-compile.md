# 76 조건부 컴파일

----

## 사용법

```c
#ifdef 매크로
코드
#elif 다른 매크로
코드
#endif
```

----

## 디버그 코드

```c
int main() {
    int a = 10;
    int b = 20;
    int c = a + b;
#ifdef DEBUG
    printf("%d %d %d\n", a, b, c);
#endif
}
```

> 디버그모드로 컴파일 했을 때만 출력

----

## 식 사용하기

```c
#define DEBUG_LEVEL 2

#if DEBUG_LEVEL >= 2
printf("DEBUG: %d\n", value);
#endif

#if 1 // Always true
puts("Print");
#endif

#if (defined DEBUG || defined TEST) && DEBUG_LEVEL > 1
// Do something
#endif
```

@[1](디버그용 매크로 세팅)
@[3-5](디버그 레벨이 2 이상인 경우 출력)
@[7-9](무조건 참이 되는 조건)
@[11-13](DEBUG 또는 TEST 매크로가 설정 되고 레벨이 1보다 크면 다음을 컴파일)

----

## Include

```c
// message.h
#if defined KO
  #define MSG "annyeong"
#elif defined EN
  #define MSG "Hello"
#elif defined EO
  #define MSG "Saluton"
#endif
```

```c
//main.c
#define EO
#include "message.h"

int main() {
    puts(MSG);
}
```
> EO를 설정했기 때문에 MSG는 "Saluton"이 됨

Result: `Saluton`
