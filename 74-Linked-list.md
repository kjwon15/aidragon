# 74 링크드리스트

----

## 기본 구조

![Linked list](imgs/linked_list.jpg)

----

## 노드 추가

![Linked list insertion](imgs/linked_list_insert.jpg)

----

## 노드 삭제

![Linked list deletion](imgs/linked_list_delete.jpg)

----

> Image source
> https://www.tutorialspoint.com/data_structures_algorithms/linked_lists_algorithm.htm

