# 75 매크로

----

매크로 미사용

```c
char s1[10];
for (int i = 0; i < 10; i += 1) {
  s1[i] = 'a' + i;
}
for (int i = 0; i < 10; i += 1) {
  printf("%c\n", s1[i]);
}
```

> 10을 12로 바꿔야 하는 경우 노가다 발생.

----

매크로 사용

```c
#define ARR_SIZE 10
char s1[ARR_SIZE];
for (int i = 0; i < ARR_SIZE; i += 1) {
  s1[i] = 'a' + i;
}
for (int i = 0; i < ARR_SIZE; i += 1) {
  printf("%c\n", s1[i]);
}
```

> 매직넘버를 사용하지 않아서 의미 부여, 수정이 용이함.

----

## 매크로 함수

`#define 이름(x) 코드`

```c
#define print_num(x) printf("%d\n", x)

print_num(10);
```

```c
printf("%d\n", 10);
```

----

## 여러 줄 매크로

```c
#define print_num3(x) printf("%d\n", x); \
printf("%d\n", x + 1); \
printf("%d\n", x + 2);

print_num(10)
```

```c
printf("%d\n", 10);
printf("%d\n", 10 + 1);
printf("%d\n", 10 + 2);
```

----

## 스왑

```c
#define swap(x, y, type) \
type temp; \
temp = x; \
x = y; \
y = temp;

int main() {
  int a = 10;
  int b = 20;
  swap(a, b, int);
  printf("%d %d\n", a, b);
  swap(a, b, int);
  printf("%d %d\n", a, b);
}
```

```c
int main() {
  int a = 10;
  int b = 20;
  int temp;
  temp = a;
  a = b;
  b = temp;
  printf("%d %d\n", a, b);
  int temp;
  temp = a;
  a = b;
  b = temp;
  printf("%d %d\n", a, b);
}
```

----

> temp 변수가 실제로 생성 되므로 같은 이름의 변수를 쓰지 못한다는 치명적인 문제 발생.
> 심지어 같은 매크로를 두 번 사용하지도 못 함.

----

문제를 해결한 스왑

```c
#define swap(x, y, type) {\
type temp; \
temp = x; \
x = y; \
y = temp; \
}
```
```c
int main() {
  int a = 10, b = 20;
  {
    int temp;
    temp = a;
    a = b;
    b = temp;
  }
  printf("%d %d\n", a, b);
}
```

> 괄호를 사용하여 스코프 제한

----

## 연산자 우선순위

```c
#define mul(x, y) x * y

int a = mul(1 + 3, 4 + 5) / 2;
// 1 + 3 * 4 + 5 / 2 → 15
```

```c
#define mul(x, y) ((x) * (y))

int a = mul(1 + 3, 4 + 5);
// ((1 + 3) * (4 + 5)) / 2 → 18
```

----

## 매크로 연결

> \#\#을 이용하면 값을 붙일 수 있음.

```c
#define concat(a, b) a##b

int a = concat(1, 2) // 12
```
```c
#define executor(n, arg) exec##n(arg)

executor(ev, "nya");
// execev("nya");
```

별로 유용하지는 않음.

----

* 매크로는 프로그래머가 코드를 읽기 좋게 만들어 줄 수 있다.
* 어디까지나 에디터에서 치환 기능을 이용하는 것과 똑같으므로 스코프 등에 주의.
* `gcc -E -dD <filename>`을 이용하면 매크로가 처리 된 소스코드를 볼 수 있다.
* `#include`도 파일 내용을 복붙 해주는 매크로.
