# 77 여러 소스파일 사용하기

----

## 소스파일을 나누는 이유

* 프로그램의 관리를 위해
* 컴파일 속도를 위해

----

## 일반적인 구조

* 기능A.c, 기능A.h
* 기능B.c, 기능B.h
* main.c

> 각 함수를 쓰려면 타입이 선언 된 헤더를 인클루드

----

## 예제

----

```c
// add.c
int add(int a, int b) {
    return a + b;
}
```

```c
// add.h
int add(int a, int b);
```

----

```c
// main.c
#include <stdio.h>
#include "add.h" // add 함수가 선언 된 헤더

int main() {
    int a = 10, b = 20;
    int result = add(a, b);
    printf("%d\n", result);
    return 0;
}
```

`gcc -o main main.c add.c`
