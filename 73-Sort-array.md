# 73 배열 정렬하기

----

## 버블소트

![Video](https://youtube.com/embed/lyZQPjUT5B4?t=52s)

----

```c
void bubbleSort(int arr[], int count) {
  int temp;
  for (int i = 0; i < count; i += 1) {
    for (int j = 0; j < count - i - 1; j += 1) {
      if (arr[j] > arr[j+1]) {
        temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
      }
    }
  }
}
```

@[2](스왑을 위한 변수)
@[3](배열 크기만큼 실행)
@[4](이미 정렬 된 곳 제외, 쌍으로 잡기 때문에 1 감소)
@[5-9](왼쪽이 오른쪽보다 크면 스왑)

----

## 퀵소트

![Video](https://www.youtube.com/embed/ywWBy6J5gz8)

----

라이브러리에 있는 함수 사용

`qsort(배열, 요소 갯수, 요소 크기, 비교함수)` — 반환 없음.

----

```c
#include <stdlib.h> //qsort

int compare(const void *a, const void *b) {
  int n1 = *(int *)a;
  int n2 = *(int *)b;
  if (n1 < n2) return 1;
  if (n1 > n2) return -1;
  return 0;
}

int main() {
  int arr[] = {8, 4, 2, 5, 3, 7, 10, 1, 6, 9};
  //    배열, 배열 크기                  , 요소 크기    , 비교함수
  qsort(arr, sizeof(arr) / sizeof(int), sizeof(int), compare);
}
```

@[1](퀵소트 사용을 위한 헤더)
@[3-9](비교용 함수, 같으면 0, 앞쪽이 크면 -1, 반대는 1)
@[13-14](퀵소트 호출)
