# 72 파일에서 구조체 읽기

----

```c
#pragma pack(push, 1) // 1byte 정렬
struct Data {
    int num1;
    int num2;
}
#pragma pack(pop) // 정렬 끝
```

----

## 파일에 구조체 쓰기

`fwrite(버퍼, 사이즈, 횟수, fp)` — 성공한 쓰기 횟수 반환.

----

```c
struct Data d1;
d1.num1 = 100;
d1.num2 = 200;

FILE *fp = fopen("data.bin", "wb");
fwrite(&d1, sizeof(d1), 1, fp);
fclose(fp);
```

@[1-3](구조체 변수 생성, 할당)
@[6](파일을 바이너리 쓰기 모드로 열기, 구조체를 파일에 쓰기)

Result: `6400 c800`

----

## 파일에서 구조체 읽기

`fread(버퍼, 사이즈, 횟수, fp)` — 성공한 읽기 횟수 반환.

----

```c
struct Data d1;

FILE *fp = fopen("data.bin", "rb");
fread(&d1, sizeof(d1), 1, fp);
printf("%d %d\n", d1.num1, d1.num2);
fclose(fp);
```

@[1](구조체 변수 생성)
@[4](파일 내용을 구조체에 로드)

Result: `100 200`

----

* 메모리에 저장되어 있는 상태 그대로 파일에 저장이 됨.
* 다른 PC로 데이터를 옮길 시 **엔디안**에 주의 — 무조건 같은 엔디안으로 바꿔 저장하는 방법 사용.
